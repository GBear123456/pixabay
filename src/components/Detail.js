import React, {Component} from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

// here the defines the modal when click card component for detail
// - User info
// - Image
// - Image infos(view, likes, downloads, and sizes)
// - Tags

class Detail extends Component {
    render()
    {
        const {isOpen, image, onCloseDlg} = this.props;
        const tags = image !== undefined && image.tags !== undefined 
                  ? image.tags.split(',') 
                  : undefined;

        return (
            <Modal                    
                isOpen={isOpen}
                toggle={onCloseDlg} 
                className="font-arial"
                style={{padding: '50px', paddingBottom:'0px'}}
            >
                <ModalHeader 
                  toggle={onCloseDlg} 
                  className="font-bold text-purple-500 text-xl mb-2"
                >
                  <span className='detail-user' style={{color:''}}>{`Photo user: ${image.user}`}</span>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close"
                    onClick={onCloseDlg}>
                    <span aria-hidden="true">&times;</span>
                  </button>
                </ModalHeader>
                <ModalBody 
                    style={{fontSize:'20px', textAlign:'center'}}
                >
                  <div className='card-image w-full py-3 px-3'>
                    <img src={image.webformatURL} className='card-img w-full'/>
                  </div>
                </ModalBody>
                <ModalFooter className="d-flex justify-content-center mb-4" style={{border:0}}>
                  <div className="px-6 py-4">
                    <ul>
                      <li>
                        <strong>Views: </strong>
                        {image.views}
                      </li>
                      <li>
                        <strong>Downloads: </strong>
                        {image.downloads}
                      </li>
                      <li>
                        <strong>Likes: </strong>
                        {image.likes}
                      </li>
                      <li>
                        <strong>Size(w x h): </strong>
                        {image.webformatWidth}px, {image.webformatHeight}px
                      </li>
                    </ul>
                  </div>
                
                  <div className="px-6 py-4">
                    <strong>Tags: </strong>
                    {tags !== undefined && tags.map((tag, index) => (
                      <span key={index} className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2">
                      #{tag}
                    </span>
                    ))}
                  </div>
                </ModalFooter>
            </Modal>
        );
    }
  }

export default Detail;
