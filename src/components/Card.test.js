import React from 'react';
import Enzyme from 'enzyme';
import { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Card from './Card';

Enzyme.configure({ adapter: new Adapter() });
const ImageData = { 
  "id":7050884,
  "pageURL":"https://pixabay.com/photos/clouds-sky-atmosphere-blue-sky-7050884/",
  "type":"photo",
  "tags":"clouds, sky, atmosphere",
  "previewURL":"https://cdn.pixabay.com/photo/2022/03/06/05/30/clouds-7050884_150.jpg",
  "previewWidth":150,
  "previewHeight":100,
  "webformatURL":"https://pixabay.com/get/ge0bec09f8c6cb0187fed31df29804312fa55e19e413d7dd29e58d263ee74bfecae10241ac17a0cf82fd5deb7af3e6523_640.jpg",
  "webformatWidth":640,
  "webformatHeight":427,
  "largeImageURL":"https://pixabay.com/get/ge68715424fc4fce29ffb9dd9dad92b7d1745e924df41674647fdc029afa849720b1c589c98a0de2f659d5e8b1e5cf708a5c1c6b5e6dc334fe7bea9d6e5106569_1280.jpg",
  "imageWidth":5184,
  "imageHeight":3456,
  "imageSize":2499645,
  "views":142212,
  "downloads":100787,
  "collections":3680,
  "likes":151,
  "comments":41,
  "user_id":25073177,
  "user":"Mint_Foto",
  "userImageURL":"",
  "id_hash":"7050884",
  "fullHDURL":"https://pixabay.com/get/g4c6e60be9924afa01db86e451889642eaf71306126a3edb211fd2d1b9feef36a0eeed7652b689c6f87681c14f8270df59942cd06ee28e03da9f088710db0036f_1920.jpg",
  "imageURL":"https://pixabay.com/get/gbddeffa325116629bb414d26fa8bb9bfa4591b822803da0909d2507c6bd3fd93952a561f66ce277540d961071e5ebd63.jpg"
}

describe('Test case for Card Component', () => {
    test('Validate no records in grid', () => {
        const wrapper = shallow(<Card image={ImageData}/>);
        const img = wrapper.find('img');
        expect(img).toHaveLength(1);
    });
});