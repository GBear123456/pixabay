import React from 'react';

// here card component consists of following
// card image
// card name
// card information(views, downloads and likes)

const Card = ({ image, onSelect }) => {
  return (
    <div className="card-container max-w-sm rounded overflow-hidden shadow-lg mx-auto" onClick={(e) => onSelect(e, image)}>
      <div className='card-image w-full py-3 px-3'>
        <img src={image.previewURL} alt="card-image" className="card-img w-full"/>
      </div>
      <div className="px-6 py-4">
        <div className="card-desc font-bold text-purple-500 text-xl mb-2">
          Photo by {image.user}
        </div>
        <ul>
          <li>
            <strong>Views: </strong>
            {image.views}
          </li>
          <li>
            <strong>Downloads: </strong>
            {image.downloads}
          </li>
          <li>
            <strong>Likes: </strong>
            {image.likes}
          </li>
        </ul>
      </div>
      
    </div>
  )
}

export default Card;
