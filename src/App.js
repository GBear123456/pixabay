import React, { useState, useEffect } from 'react';
import Card from './components/Card';
import Pagination from 'react-js-pagination';
import Detail from './components/Detail';

function App() {
  // states
  // card related states
  const [imageCount, setImageCount] = useState(0);
  const [images, setImages] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  // pagenation related states
  const [pageNum, setPageNum] = useState(1);
  const [perPage, setPerPage] = useState(12);
  // for modaling states
  const [selectedImg, setSelectedImage] = useState('');
  const [openDlg, setOpenDlg] = useState(false);

  // pulling images from backend... (according to the page number)
  useEffect(() => {
    fetch(`https://pixabay.com/api/?key=${process.env.REACT_APP_PIXABAY_API_KEY}&per_page=12&page=${pageNum}`)
      .then(res => res.json())
      .then(data => {
        setImageCount(data.totalHits);
        setImages(data.hits);
        setIsLoading(false);
      })
      .catch(err => console.log(err));
  }, [pageNum]);

  // when page is changed....
  const handleSelected = (pageIndex) => {
    setPageNum(pageIndex);
    console.log('pageIndex => ', pageIndex);
  }

  // when item(card) is clicked...
  const onSelectImage = (e, image) => {
    setSelectedImage(image);
    setOpenDlg(true);
    console.log('image select => ', image)
  }

  // close opened modal dlg...
  const onCloseDialog = () => {
    setOpenDlg(false);
  }

  // hear you must consult the loading state and image length state...
  return (
    <div className='container mx-auto mt-5'>
      <div className='font-bold text-purple-500 text-xl mb-2 py-3'>Pixabay Find Results</div>
      {!isLoading && images.length === 0 && <h1 className='text-5xl text-center mx-auto mt-32'>No Images Found</h1> }

      {isLoading 
      ? <h1 className='text-6xl text-center mx-auto mt-32'>Loading...</h1> 
      : 
      <div className='grid grid-custom gap-4'>
        {images.map(image => (
            <Card key={image.id} image={image} onSelect={onSelectImage}/>
        ))}
      </div>
      }
      {
        images.length > 0 &&   
        <div className='items-center border-b border-b-2 border-teal-500 py-2 row' style={{display: 'flex'}}>
            <div style={{marginLeft:'auto', marginRight: 'auto'}}>
                <Pagination
                    className='flex'
                    style={{ float:'left'}}
                    totalItemsCount={imageCount}
                    onChange={handleSelected}
                    activePage={pageNum}
                    itemsCountPerPage={perPage}
                    pageRangeDisplayed={5}
                />
            </div>
        </div>
      }
	  <Detail isOpen={openDlg} image={selectedImg} onCloseDlg={onCloseDialog}/>
    </div>
  );
}

export default App;
